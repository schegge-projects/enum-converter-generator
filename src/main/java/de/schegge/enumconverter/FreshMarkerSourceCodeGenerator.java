package de.schegge.enumconverter;

import ftl.ParseException;
import org.freshmarker.Configuration;
import org.freshmarker.Template;
import org.freshmarker.core.plugin.BooleanPluginProvider;
import org.freshmarker.core.plugin.DatePluginProvider;
import org.freshmarker.core.plugin.EnumPluginProvider;
import org.freshmarker.core.plugin.LooperPluginProvider;
import org.freshmarker.core.plugin.NumberPluginProvider;
import org.freshmarker.core.plugin.SequencePluginProvider;
import org.freshmarker.core.plugin.StringPluginProvider;
import org.freshmarker.core.plugin.SystemPluginProvider;
import org.freshmarker.core.plugin.TemporalPluginProvider;

import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Scanner;
import java.util.stream.Stream;

import static java.nio.charset.StandardCharsets.UTF_8;

public class FreshMarkerSourceCodeGenerator implements SourceCodeGenerator {

    private final Template template;
    private final Map<String, Object> model;

    public FreshMarkerSourceCodeGenerator(Class<?> baseClass, String name, String version) {
        Configuration configuration = new Configuration();
        Stream.of(new StringPluginProvider(), new BooleanPluginProvider(), new NumberPluginProvider(), new TemporalPluginProvider(), new DatePluginProvider(),
                        new LooperPluginProvider(), new EnumPluginProvider(), new SequencePluginProvider(), new SystemPluginProvider())
                .forEach(configuration::registerPlugin);
        String templateContent = new Scanner(baseClass.getResourceAsStream("template.ftl"), UTF_8).useDelimiter("\\A").next();
        model = Map.of("author", Objects.requireNonNull(name) + " by Jens Kaiser", "version", Objects.requireNonNull(version));
        template = configuration.builder().getTemplate("template.ftl", templateContent);
    }

    @Override
    public void processTemplate(Map<String, Object> root, PrintWriter out) {
        Map<String, Object> model = new HashMap<>(root);
        model.putAll(this.model);
        try {
            template.process(model, out);
        } catch (ParseException e) {
            throw new IllegalArgumentException(e);
        }
    }
}
