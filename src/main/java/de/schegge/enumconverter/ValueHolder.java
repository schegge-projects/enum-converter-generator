package de.schegge.enumconverter;

import java.util.List;

public record ValueHolder(String key, List<String> value, boolean plain) {

  @Override
  public String toString() {
    return key + "=" + value + " (" + plain + ")";
  }
}
