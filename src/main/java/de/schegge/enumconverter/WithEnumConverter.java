package de.schegge.enumconverter;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface WithEnumConverter {

  boolean ordinal() default true;

  boolean autoApply() default false;

  boolean nullKeyForbidden() default false;

  boolean exceptionIfMissing() default false;
}
