package de.schegge.enumconverter;

import com.google.auto.service.AutoService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;
import javax.annotation.processing.AbstractProcessor;
import javax.annotation.processing.ProcessingEnvironment;
import javax.annotation.processing.Processor;
import javax.annotation.processing.RoundEnvironment;
import javax.annotation.processing.SupportedAnnotationTypes;
import javax.annotation.processing.SupportedSourceVersion;
import javax.lang.model.SourceVersion;
import javax.lang.model.element.AnnotationMirror;
import javax.lang.model.element.AnnotationValue;
import javax.lang.model.element.Element;
import javax.lang.model.element.ElementKind;
import javax.lang.model.element.TypeElement;
import javax.lang.model.type.TypeMirror;
import javax.lang.model.util.Elements;
import javax.tools.Diagnostic.Kind;

@SupportedAnnotationTypes({"de.schegge.enumconverter.WithEnumConverter"})
@SupportedSourceVersion(SourceVersion.RELEASE_21)
@AutoService(Processor.class)
public class EnumConverterProcessor extends AbstractProcessor {

  private static final Logger log = LoggerFactory.getLogger(EnumConverterProcessor.class);
  private EnumConverterWriter writer;

  private TypeMirror converterAnnotation;
  private TypeMirror valueAnnotation;

  @Override
  public synchronized void init(ProcessingEnvironment processingEnv) {
    super.init(processingEnv);
    Elements elementUtils = processingEnv.getElementUtils();
    writer = new EnumConverterWriter(processingEnv, elementUtils);
    converterAnnotation = elementUtils.getTypeElement(WithEnumConverter.class.getCanonicalName()).asType();
    valueAnnotation = elementUtils.getTypeElement(ConverterValue.class.getCanonicalName()).asType();
  }

  @Override
  public boolean process(Set<? extends TypeElement> annotations, RoundEnvironment roundEnv) {
    try {
      printMessage(Kind.NOTE, "round: " + roundEnv.getRootElements());
      return annotations.stream().map(roundEnv::getElementsAnnotatedWith).flatMap(Set::stream)
          .map(TypeElement.class::cast).map(this::validateEnumType).allMatch(this::processEnumForConverter);
    } catch (IllegalArgumentException e) {
      printMessage(Kind.ERROR, e.getMessage());
      return false;
    }
  }

  private boolean processEnumForConverter(TypeElement enumType) {
    AnnotationMirror enumTypeMirror = getConverterAnnotation(enumType).orElseThrow(IllegalArgumentException::new);
    boolean ordinal = getOptionalBoolean("ordinal", enumTypeMirror).orElse(true);
    boolean autoApply = getOptionalBoolean("autoApply", enumTypeMirror).orElse(false);
    boolean exceptionIfMissing = getOptionalBoolean("exceptionIfMissing", enumTypeMirror).orElse(false);
    boolean nullKeyForbidden = getOptionalBoolean("nullKeyForbidden", enumTypeMirror).orElse(false);
    List<ValueHolder> list = createValueHolders(enumType, ordinal);
    return writer.createEnumConverterClassFile(enumType, ordinal, list, autoApply, exceptionIfMissing, nullKeyForbidden);
  }

  private List<ValueHolder> createValueHolders(TypeElement enumType, boolean ordinal) {
    AtomicInteger index = new AtomicInteger();
    List<ValueHolder> list = enumType.getEnclosedElements().stream()
        .filter(x -> x.getKind() == ElementKind.ENUM_CONSTANT).map(x -> convert(x, index, ordinal))
        .filter(Objects::nonNull).toList();
    if (list.stream().allMatch(ValueHolder::plain)) {
      printMessage(Kind.WARNING, ordinal ? "use @Enumerated" : "use @Enumerated(EnumType.STRING)");
    }
    Map<String, List<String>> values = list.stream().map(ValueHolder::value).flatMap(List::stream)
        .collect(Collectors.groupingBy(String::toString));
    List<String> duplicates = values.values().stream().filter(v -> v.size() != 1).flatMap(Collection::stream).toList();
    if (!duplicates.isEmpty()) {
      throw new IllegalArgumentException("duplicates defined: " + duplicates);
    }
    printMessage(Kind.NOTE, "values: " + list);
    return list;
  }

  private TypeElement validateEnumType(TypeElement enumType) {
    if (enumType.getKind() != ElementKind.ENUM) {
      throw new IllegalArgumentException("annotated type is no enum: " + enumType);
    }
    return enumType;
  }

  private ValueHolder convert(Element enumConstant, AtomicInteger index, boolean ordinal) {
    String currentIndex = String.valueOf(index.incrementAndGet());
    String name = enumConstant.getSimpleName().toString();
    Optional<? extends AnnotationMirror> optionalConverterValue = enumConstant.getAnnotationMirrors().stream()
        .filter(x -> x.getAnnotationType().equals(valueAnnotation)).findFirst();
    if (optionalConverterValue.isEmpty()) {
      String value = ordinal ? currentIndex : '"' + name + '"';
      return new ValueHolder(name, List.of(value), true);
    }
    AnnotationMirror converterValue = optionalConverterValue.get();
    boolean ignored = getOptionalBoolean("ignored", converterValue).orElse(false);
    if (ignored) {
      return null;
    }
    boolean include = getOptionalBoolean("include", converterValue).orElse(false);
    List<String> values = converterValue.getElementValues().entrySet().stream()
        .filter(e -> "value".equals(e.getKey().getSimpleName().toString()))
        .map(Entry::getValue).findFirst().stream()
        .map(a -> a.accept(new StringListAnnotationValueVisitor(), null)).flatMap(List::stream).collect(Collectors.toList());
    String includeValue = ordinal ? currentIndex : name;
    log.info("include: {} {} {}", include, includeValue, values);
    if (include && !values.contains(includeValue)) {
      values.add(includeValue);
    }
    if (values.isEmpty()) {
      throw new IllegalArgumentException("values is empty: " + name);
    }

    if (ordinal) {
      return new ValueHolder(name, values.stream().map(v -> String.valueOf(Integer.valueOf(v))).toList(), false);
    }
    return new ValueHolder(name, values.stream().map(v -> '"' + v + '"').toList(), false);
  }

  private Optional<AnnotationMirror> getConverterAnnotation(TypeElement enumType) {
    return enumType.getAnnotationMirrors().stream().filter(tm -> tm.getAnnotationType().equals(converterAnnotation))
        .map(AnnotationMirror.class::cast).findFirst();
  }

  private void printMessage(Kind kind, Object value) {
    processingEnv.getMessager().printMessage(kind, String.valueOf(value));
  }

  private Optional<Boolean> getOptionalBoolean(String key, AnnotationMirror annotationMirror) {
    return annotationMirror.getElementValues().entrySet().stream()
        .filter(e -> key.equals(e.getKey().getSimpleName().toString())).map(Entry::getValue).findFirst()
        .stream().map(a -> a.accept(new BooleanAnnotationValueVisitor(), null)).filter(Objects::nonNull).findFirst();
  }

  private static class BooleanAnnotationValueVisitor extends AbstractAnnotationValueVisitor<Boolean, Void> {

    @Override
    public Boolean visitBoolean(boolean b, Void unused) {
      return b;
    }
  }

  private static class StringListAnnotationValueVisitor extends AbstractAnnotationValueVisitor<List<String>, Void> {

    @Override
    public List<String> visitString(String s, Void unused) {
      return List.of(s);
    }

    @Override
    public List<String> visitArray(List<? extends AnnotationValue> vals, Void unused) {
      return vals.stream().map(v -> v.accept(this, null)).flatMap(List::stream).toList();
    }
  }
}