package de.schegge.enumconverter;

import java.io.PrintWriter;
import java.util.Map;

public interface SourceCodeGenerator {

  void processTemplate(Map<String, Object> root, PrintWriter out);
}
