package de.schegge.enumconverter;

import com.google.common.collect.ImmutableList;
import com.google.testing.compile.Compilation;
import com.google.testing.compile.Compilation.Status;
import com.google.testing.compile.Compiler;
import com.google.testing.compile.JavaFileObjects;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import javax.tools.JavaFileObject;
import java.io.IOException;
import java.util.Locale;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertSame;

class WithEnumConverterProcessorTest {

    private Compiler compiler;

    @BeforeEach
    void setUp() {
        compiler = Compiler.javac().withProcessors(new EnumConverterProcessor());
    }

    @Test
    void withoutAnnotation() {
        Compilation compilation = compiler.compile(JavaFileObjects.forSourceString("TestEnum", "public enum TestEnum {}"));
        assertSame(Status.SUCCESS, compilation.status());
    }

    @Test
    void withIntegerRepresentation() throws IOException {
        Compilation compilation = compiler.compile(JavaFileObjects.forSourceString("TestEnum",
                """
                        package de.schegge;
                        import de.schegge.enumconverter.WithEnumConverter;
                                                
                        @WithEnumConverter
                        enum TestEnum {
                          A, B, C
                        }"""));
        compilation.notes().forEach(System.out::println);
        JavaFileObject javaFileObject = compilation.generatedSourceFiles().getFirst();
        assertEquals("/SOURCE_OUTPUT/de/schegge/TestEnumConverter.java", javaFileObject.getName());
        String charContent = javaFileObject.getCharContent(true).toString().replaceAll("[\\r]+", "");
        assertEquals(
                """
                        /**
                        * @author Enum Converter Generator by Jens Kaiser
                        * @version 2.3.0
                        */
                        package de.schegge;

                        import javax.persistence.AttributeConverter;
                        import javax.persistence.Converter;
                        import java.util.Map;
                        import java.util.HashMap;
                        import java.util.EnumMap;

                        @Converter
                        public final class TestEnumConverter implements AttributeConverter<TestEnum, Integer> {

                          private final Map<TestEnum, Integer> toDatabaseColumn = new EnumMap<>(TestEnum.class);
                          private final Map<Integer, TestEnum> toEntityAttribute = new HashMap<>();

                          public TestEnumConverter() {
                            toDatabaseColumn.put(TestEnum.A, 1);
                            toDatabaseColumn.put(TestEnum.B, 2);
                            toDatabaseColumn.put(TestEnum.C, 3);

                            toEntityAttribute.put(1, TestEnum.A);
                            toEntityAttribute.put(2, TestEnum.B);
                            toEntityAttribute.put(3, TestEnum.C);
                          }

                          @Override
                          public Integer convertToDatabaseColumn(TestEnum value) {
                            if (value == null) {
                              return null;
                            }
                            return toDatabaseColumn.get(value);
                          }
                                                
                          @Override
                          public TestEnum convertToEntityAttribute(Integer value) {
                            if (value == null) {
                              return null;
                            }
                            return toEntityAttribute.get(value);
                          }
                        }""", charContent);
        compilation.errors().forEach(System.out::println);
        assertSame(Status.SUCCESS, compilation.status());
    }

    @Test
    void checkWarning() {
        Compilation compilation = compiler.compile(JavaFileObjects.forSourceString("TestEnum",
                """
                        package de.schegge;
                        import de.schegge.enumconverter.WithEnumConverter;
                                                
                        @WithEnumConverter
                        enum TestEnum {
                          A, B, C
                        }"""));
        assertEquals("use @Enumerated", compilation.warnings().getFirst().getMessage(Locale.getDefault()));
        assertSame(Status.SUCCESS, compilation.status());
    }

    @Test
    void withAutoApply() {
        Compilation compilation = compiler.compile(JavaFileObjects.forSourceString("TestEnum",
                """
                        package de.schegge;
                        import de.schegge.enumconverter.WithEnumConverter;
                                                
                        @WithEnumConverter(autoApply=true)
                        enum TestEnum {
                          A, B, C
                        }"""));
        compilation.notes().forEach(System.out::println);
        compilation.generatedSourceFiles().forEach(i -> {
            System.out.println(i.getName());
            try {
                System.out.println(i.getCharContent(true));
            } catch (IOException e) {
                System.out.println("ERROR " + e);
            }
        });
        compilation.errors().forEach(System.out::println);
        assertSame(Status.SUCCESS, compilation.status());
    }

    @Test
    void withIntegerRepresentationAndIgnored() {
        Compilation compilation = compiler.compile(JavaFileObjects.forSourceString("TestEnum",
                """
                        package de.schegge;
                        import de.schegge.enumconverter.WithEnumConverter;
                        import de.schegge.enumconverter.ConverterValue;
                                                
                        @WithEnumConverter
                        enum TestEnum {
                          @ConverterValue(ignored=true) A, B, C
                        }"""));
        compilation.notes().forEach(System.out::println);
        compilation.generatedSourceFiles().forEach(i -> {
            System.out.println(i.getName());
            try {
                System.out.println(i.getCharContent(true));
            } catch (IOException e) {
                System.out.println("ERROR " + e);
            }
        });
        compilation.errors().forEach(System.out::println);
        assertSame(Status.SUCCESS, compilation.status());
    }

    @Test
    void withIntegerRepresentationAndConverterValue() {
        Compilation compilation = compiler.compile(JavaFileObjects.forSourceString("TestEnum",
                """
                        package de.schegge;
                        import de.schegge.enumconverter.WithEnumConverter;
                        import de.schegge.enumconverter.ConverterValue;
                                    
                        @WithEnumConverter
                        enum TestEnum {
                          A, B, @ConverterValue(value = {"4","999"}, include=true) C
                        }"""));
        compilation.notes().forEach(System.out::println);
        compilation.generatedSourceFiles().forEach(i -> {
            System.out.println(i.getName());
            try {
                System.out.println(i.getCharContent(true));
            } catch (IOException e) {
                System.out.println("ERROR " + e);
            }
        });
        compilation.errors().forEach(System.out::println);
        assertSame(Status.SUCCESS, compilation.status());
    }


    @Test
    void withStringRepresentation() {
        Compilation compilation = compiler.compile(JavaFileObjects.forSourceString("TestEnum",
                """
                        package de.schegge;
                        import de.schegge.enumconverter.WithEnumConverter;
                                    
                        @WithEnumConverter(ordinal=false)
                        enum TestEnum {
                          A, B, C
                        }"""));
        compilation.notes().forEach(System.out::println);
        compilation.generatedSourceFiles().forEach(i -> {
            System.out.println(i.getName());
            try {
                System.out.println(i.getCharContent(true));
            } catch (IOException e) {
                System.out.println("ERROR " + e);
            }
        });
        compilation.errors().forEach(System.out::println);
        assertSame(Status.SUCCESS, compilation.status());
    }

    @Test
    void withConverterValue() {
        Compilation compilation = compiler.compile(JavaFileObjects.forSourceString("TestEnum",
                """
                        package de.schegge;
                        import de.schegge.enumconverter.WithEnumConverter;
                        import de.schegge.enumconverter.ConverterValue;
                                    
                        @WithEnumConverter(ordinal=false)
                        enum TestEnum {
                          @ConverterValue("a")
                          A,
                          @ConverterValue("b")
                          B
                        }"""));
        compilation.notes().forEach(System.out::println);
        compilation.generatedSourceFiles().forEach(i -> {
            System.out.println(i.getName());
            try {
                System.out.println(i.getCharContent(true));
            } catch (IOException e) {
                System.out.println("ERROR " + e);
            }
        });
        compilation.errors().forEach(System.out::println);
        assertSame(Status.SUCCESS, compilation.status());
    }

    @Test
    void withMultipleConverterValue() throws IOException {
        Compilation compilation = compiler.compile(JavaFileObjects.forSourceString("TestEnum",
                """
                        package de.schegge;
                        import de.schegge.enumconverter.WithEnumConverter;
                        import de.schegge.enumconverter.ConverterValue;
                                    
                        @WithEnumConverter(ordinal=false)
                        enum TestEnum {
                          @ConverterValue({"a", "legacyA"})
                          A,
                          B,
                          @ConverterValue(ignored=true)
                          C
                        }"""));
        compilation.notes().forEach(System.out::println);
        JavaFileObject javaFileObject = compilation.generatedSourceFiles().getFirst();
        assertEquals("/SOURCE_OUTPUT/de/schegge/TestEnumConverter.java", javaFileObject.getName());
        String charContent = javaFileObject.getCharContent(true).toString().replaceAll("[\\r]+", "");
        assertEquals(
                """
                        /**
                        * @author Enum Converter Generator by Jens Kaiser
                        * @version 2.3.0
                        */
                        package de.schegge;

                        import javax.persistence.AttributeConverter;
                        import javax.persistence.Converter;
                        import java.util.Map;
                        import java.util.HashMap;
                        import java.util.EnumMap;

                        @Converter
                        public final class TestEnumConverter implements AttributeConverter<TestEnum, String> {

                          private final Map<TestEnum, String> toDatabaseColumn = new EnumMap<>(TestEnum.class);
                          private final Map<String, TestEnum> toEntityAttribute = new HashMap<>();

                          public TestEnumConverter() {
                            toDatabaseColumn.put(TestEnum.A, "a");
                            toDatabaseColumn.put(TestEnum.B, "B");

                            toEntityAttribute.put("a", TestEnum.A);
                            toEntityAttribute.put("legacyA", TestEnum.A);
                            toEntityAttribute.put("B", TestEnum.B);
                          }

                          @Override
                          public String convertToDatabaseColumn(TestEnum value) {
                            if (value == null) {
                              return null;
                            }
                            return toDatabaseColumn.get(value);
                          }
                                    
                          @Override
                          public TestEnum convertToEntityAttribute(String value) {
                            if (value == null) {
                              return null;
                            }
                            return toEntityAttribute.get(value);
                          }
                        }""", charContent);
        compilation.errors().forEach(System.out::println);
        assertSame(Status.SUCCESS, compilation.status());
    }

    @Test
    void withMultipleConverterValueAndInclude() throws IOException {
        Compilation compilation = compiler.compile(JavaFileObjects.forSourceString("TestEnum",
                """
                        package de.schegge;
                        import de.schegge.enumconverter.WithEnumConverter;
                        import de.schegge.enumconverter.ConverterValue;
                                    
                        @WithEnumConverter(ordinal=false)
                        enum TestEnum {
                          @ConverterValue(value={"a", "legacyA"}, include=true)
                          A,
                          B,
                          @ConverterValue(ignored=true)
                          C
                        }"""));
        compilation.notes().forEach(System.out::println);
        JavaFileObject javaFileObject = compilation.generatedSourceFiles().getFirst();
        assertEquals("/SOURCE_OUTPUT/de/schegge/TestEnumConverter.java", javaFileObject.getName());
        String charContent = javaFileObject.getCharContent(true).toString().replaceAll("[\\r]+", "");
        assertEquals(
                """
                        /**
                        * @author Enum Converter Generator by Jens Kaiser
                        * @version 2.3.0
                        */
                        package de.schegge;

                        import javax.persistence.AttributeConverter;
                        import javax.persistence.Converter;
                        import java.util.Map;
                        import java.util.HashMap;
                        import java.util.EnumMap;

                        @Converter
                        public final class TestEnumConverter implements AttributeConverter<TestEnum, String> {

                          private final Map<TestEnum, String> toDatabaseColumn = new EnumMap<>(TestEnum.class);
                          private final Map<String, TestEnum> toEntityAttribute = new HashMap<>();

                          public TestEnumConverter() {
                            toDatabaseColumn.put(TestEnum.A, "a");
                            toDatabaseColumn.put(TestEnum.B, "B");

                            toEntityAttribute.put("a", TestEnum.A);
                            toEntityAttribute.put("legacyA", TestEnum.A);
                            toEntityAttribute.put("A", TestEnum.A);
                            toEntityAttribute.put("B", TestEnum.B);
                          }

                          @Override
                          public String convertToDatabaseColumn(TestEnum value) {
                            if (value == null) {
                              return null;
                            }
                            return toDatabaseColumn.get(value);
                          }
                                    
                          @Override
                          public TestEnum convertToEntityAttribute(String value) {
                            if (value == null) {
                              return null;
                            }
                            return toEntityAttribute.get(value);
                          }
                        }""", charContent);
        compilation.errors().forEach(System.out::println);
        assertSame(Status.SUCCESS, compilation.status());
    }

    @Test
    void withNullKeysForbidden() throws IOException {
        Compilation compilation = compiler.compile(JavaFileObjects.forSourceString("TestEnum",
                """
                        package de.schegge;
                        import de.schegge.enumconverter.WithEnumConverter;
                        import de.schegge.enumconverter.ConverterValue;
                                    
                        @WithEnumConverter(ordinal=false, nullKeyForbidden=true)
                        enum TestEnum {
                          @ConverterValue({"a", "legacyA"})
                          A,
                          B,
                          @ConverterValue(ignored=true)
                          C
                        }"""));
        compilation.notes().forEach(System.out::println);
        ImmutableList<JavaFileObject> javaFileObjects = compilation.generatedSourceFiles();
        assertEquals(1, javaFileObjects.size());
        JavaFileObject javaFileObject = javaFileObjects.getFirst();
        assertEquals("/SOURCE_OUTPUT/de/schegge/TestEnumConverter.java", javaFileObject.getName());
        String charContent = javaFileObject.getCharContent(true).toString().replaceAll("[\\r]+", "");
        System.out.println(charContent);
        assertEquals(
                """
                        /**
                        * @author Enum Converter Generator by Jens Kaiser
                        * @version 2.3.0
                        */
                        package de.schegge;

                        import javax.persistence.AttributeConverter;
                        import javax.persistence.Converter;
                        import java.util.Map;
                        import java.util.HashMap;
                        import java.util.EnumMap;

                        @Converter
                        public final class TestEnumConverter implements AttributeConverter<TestEnum, String> {

                          private final Map<TestEnum, String> toDatabaseColumn = new EnumMap<>(TestEnum.class);
                          private final Map<String, TestEnum> toEntityAttribute = new HashMap<>();

                          public TestEnumConverter() {
                            toDatabaseColumn.put(TestEnum.A, "a");
                            toDatabaseColumn.put(TestEnum.B, "B");

                            toEntityAttribute.put("a", TestEnum.A);
                            toEntityAttribute.put("legacyA", TestEnum.A);
                            toEntityAttribute.put("B", TestEnum.B);
                          }

                          @Override
                          public String convertToDatabaseColumn(TestEnum value) {
                            if (value == null) {
                              throw new IllegalArgumentException("null key is forbidden");
                            }
                            return toDatabaseColumn.get(value);
                          }
                                    
                          @Override
                          public TestEnum convertToEntityAttribute(String value) {
                            if (value == null) {
                              throw new IllegalArgumentException("null key is forbidden");
                            }
                            return toEntityAttribute.get(value);
                          }
                        }""", charContent);
        compilation.errors().forEach(System.out::println);
        assertSame(Status.SUCCESS, compilation.status());
    }

    @Test
    void withExceptionIfMissing() throws IOException {
        Compilation compilation = compiler.compile(JavaFileObjects.forSourceString("TestEnum",
                """
                        package de.schegge;
                        import de.schegge.enumconverter.WithEnumConverter;
                        import de.schegge.enumconverter.ConverterValue;
                                    
                        @WithEnumConverter(ordinal=false, exceptionIfMissing=true)
                        enum TestEnum {
                          @ConverterValue({"a", "legacyA"})
                          A,
                          B,
                          @ConverterValue(ignored=true)
                          C
                        }"""));
        compilation.notes().forEach(System.out::println);
        ImmutableList<JavaFileObject> javaFileObjects = compilation.generatedSourceFiles();
        assertEquals(1, javaFileObjects.size());
        JavaFileObject javaFileObject = javaFileObjects.getFirst();
        assertEquals("/SOURCE_OUTPUT/de/schegge/TestEnumConverter.java", javaFileObject.getName());
        String charContent = javaFileObject.getCharContent(true).toString();
        System.out.println(charContent);
        assertEquals(
                """
                        /**
                        * @author Enum Converter Generator by Jens Kaiser
                        * @version 2.3.0
                        */
                        package de.schegge;

                        import javax.persistence.AttributeConverter;
                        import javax.persistence.Converter;
                        import java.util.Map;
                        import java.util.HashMap;
                        import java.util.EnumMap;

                        @Converter
                        public final class TestEnumConverter implements AttributeConverter<TestEnum, String> {

                          private final Map<TestEnum, String> toDatabaseColumn = new EnumMap<>(TestEnum.class);
                          private final Map<String, TestEnum> toEntityAttribute = new HashMap<>();

                          public TestEnumConverter() {
                            toDatabaseColumn.put(TestEnum.A, "a");
                            toDatabaseColumn.put(TestEnum.B, "B");

                            toEntityAttribute.put("a", TestEnum.A);
                            toEntityAttribute.put("legacyA", TestEnum.A);
                            toEntityAttribute.put("B", TestEnum.B);
                          }

                          @Override
                          public String convertToDatabaseColumn(TestEnum value) {
                            if (value == null) {
                              return null;
                            }
                            return check(value, toDatabaseColumn.get(value));
                          }
                                    
                          @Override
                          public TestEnum convertToEntityAttribute(String value) {
                            if (value == null) {
                              return null;
                            }
                            return check(value, toEntityAttribute.get(value));
                          }

                          private <K,V> V check(K key, V value) {
                            if (value == null) {
                              throw new IllegalArgumentException("null value is forbidden: " + key);
                            }
                            return value;
                          }
                        }""", charContent);
        compilation.errors().forEach(System.out::println);
        assertSame(Status.SUCCESS, compilation.status());
    }

    @Test
    void readme() throws IOException {
        Compilation compilation = compiler.compile(JavaFileObjects.forSourceString("TestEnum",
                """
                        package de.schegge;
                        import de.schegge.enumconverter.WithEnumConverter;
                        import de.schegge.enumconverter.ConverterValue;
                                        
                        @WithEnumConverter(ordinal=false)
                        enum ErrorCode {
                          @ConverterValue(ignored=true)
                          UNKNOWN,
                          @ConverterValue("400")
                          BAD_REQUEST,
                          @ConverterValue("404")
                          FILE_NOT_FOUND,
                          @ConverterValue({"401", "403"})
                          SECURITY,
                        }
                        """));
        compilation.notes().forEach(System.out::println);
        ImmutableList<JavaFileObject> javaFileObjects = compilation.generatedSourceFiles();
        assertEquals(1, javaFileObjects.size());
        JavaFileObject javaFileObject = javaFileObjects.getFirst();
        assertEquals("/SOURCE_OUTPUT/de/schegge/ErrorCodeConverter.java", javaFileObject.getName());
        String charContent = javaFileObject.getCharContent(true).toString();
        System.out.println(charContent);
        assertEquals("""
                /**
                * @author Enum Converter Generator by Jens Kaiser
                * @version 2.3.0
                */
                package de.schegge;
                                
                import javax.persistence.AttributeConverter;
                import javax.persistence.Converter;
                import java.util.Map;
                import java.util.HashMap;
                import java.util.EnumMap;
                            
                @Converter
                public final class ErrorCodeConverter implements AttributeConverter<ErrorCode, String> {
                            
                  private final Map<ErrorCode, String> toDatabaseColumn = new EnumMap<>(ErrorCode.class);
                  private final Map<String, ErrorCode> toEntityAttribute = new HashMap<>();
                            
                  public ErrorCodeConverter() {
                    toDatabaseColumn.put(ErrorCode.BAD_REQUEST, "400");
                    toDatabaseColumn.put(ErrorCode.FILE_NOT_FOUND, "404");
                    toDatabaseColumn.put(ErrorCode.SECURITY, "401");
                            
                    toEntityAttribute.put("400", ErrorCode.BAD_REQUEST);
                    toEntityAttribute.put("404", ErrorCode.FILE_NOT_FOUND);
                    toEntityAttribute.put("401", ErrorCode.SECURITY);
                    toEntityAttribute.put("403", ErrorCode.SECURITY);
                  }
                            
                  @Override
                  public String convertToDatabaseColumn(ErrorCode value) {
                    if (value == null) {
                      return null;
                    }
                    return toDatabaseColumn.get(value);
                  }
                            
                  @Override
                  public ErrorCode convertToEntityAttribute(String value) {
                    if (value == null) {
                      return null;
                    }
                    return toEntityAttribute.get(value);
                  }
                }""", charContent);
        compilation.errors().forEach(System.out::println);
        assertSame(Status.SUCCESS, compilation.status());
    }

    @Test
    void medium() throws IOException {
        Compilation compilation = compiler.compile(JavaFileObjects.forSourceString("FormatStyle",
                """
                        package de.schegge;
                        import de.schegge.enumconverter.WithEnumConverter;
                        import de.schegge.enumconverter.ConverterValue;
                                        
                        @WithEnumConverter(ordinal=false)
                        public enum FormatStyle {
                            @ConverterValue({"LONG", "LEGACY"})
                            FULL,
                            @ConverterValue(value="FULL", include=false)
                            LONG,
                            MEDIUM,
                            @ConverterValue({"SHORT", "TINY"})
                            SHORT;
                        }
                        """));
        compilation.notes().forEach(System.out::println);
        ImmutableList<JavaFileObject> javaFileObjects = compilation.generatedSourceFiles();
        assertEquals(1, javaFileObjects.size());
        JavaFileObject javaFileObject = javaFileObjects.getFirst();
        assertEquals("/SOURCE_OUTPUT/de/schegge/FormatStyleConverter.java", javaFileObject.getName());
        String charContent = javaFileObject.getCharContent(true).toString();
        System.out.println(charContent);
        assertEquals(
                """
                        /**
                        * @author Enum Converter Generator by Jens Kaiser
                        * @version 2.3.0
                        */
                        package de.schegge;
                                                
                        import javax.persistence.AttributeConverter;
                        import javax.persistence.Converter;
                        import java.util.Map;
                        import java.util.HashMap;
                        import java.util.EnumMap;
                                                
                        @Converter
                        public final class FormatStyleConverter implements AttributeConverter<FormatStyle, String> {
                                                
                          private final Map<FormatStyle, String> toDatabaseColumn = new EnumMap<>(FormatStyle.class);
                          private final Map<String, FormatStyle> toEntityAttribute = new HashMap<>();
                                                
                          public FormatStyleConverter() {
                            toDatabaseColumn.put(FormatStyle.FULL, "LONG");
                            toDatabaseColumn.put(FormatStyle.LONG, "FULL");
                            toDatabaseColumn.put(FormatStyle.MEDIUM, "MEDIUM");
                            toDatabaseColumn.put(FormatStyle.SHORT, "SHORT");
                                                
                            toEntityAttribute.put("LONG", FormatStyle.FULL);
                            toEntityAttribute.put("LEGACY", FormatStyle.FULL);
                            toEntityAttribute.put("FULL", FormatStyle.LONG);
                            toEntityAttribute.put("MEDIUM", FormatStyle.MEDIUM);
                            toEntityAttribute.put("SHORT", FormatStyle.SHORT);
                            toEntityAttribute.put("TINY", FormatStyle.SHORT);
                          }
                                                
                          @Override
                          public String convertToDatabaseColumn(FormatStyle value) {
                            if (value == null) {
                              return null;
                            }
                            return toDatabaseColumn.get(value);
                          }
                                                
                          @Override
                          public FormatStyle convertToEntityAttribute(String value) {
                            if (value == null) {
                              return null;
                            }
                            return toEntityAttribute.get(value);
                          }
                        }""", charContent);
        compilation.errors().forEach(System.out::println);
        assertSame(Status.SUCCESS, compilation.status());
    }
}