= Enum Converter Generator

This Java 21 Annotation Processor generates Attribute Converters from annotated Enum classes.
This project is part of the blog articles https://schegge.de/2022/10/automatisch-generierte-enum-converter/[Automatisch generierte Enum Converter] and https://schegge.de/2022/10/automatisch-generierte-enum-converter-2/[Automatisch generierte Enum Converter (2)].

== Usage

The name of the converter is the name of the enum with the suffix `Converter`.

[source,xml]
----
<dependency>
  <groupId>de.schegge</groupId>
  <artifactId>enum-converter-generator</artifactId>
  <version>2.0.4</versio>
</dependency>
----

== Examples

=== Numeric Enum Converter

==== Source Enum

[source,java]
----
package org.example;

import de.schegge.enumconverter.WithEnumConverter;
import de.schegge.enumconverter.ConverterValue;

@WithEnumConverter
enum ErrorCode {
  UNKNOWN,
  @ConverterValue("400")
  BAD_REQUEST,
  @ConverterValue("404")
  FILE_NOT_FOUND
}
----

==== Generated Converter

[source,java]
----
/**
* @author Enum Converter Generator by Jens Kaiser
* @version 2.0.0
*/
package org.example;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import java.util.Map;
import java.util.HashMap;

@Converter
public final class ErrorCodeConverter implements AttributeConverter<ErrorCode, Integer> {

  private final Map<ErrorCode, Integer> toDatabaseColumn = new HashMap<>();
  private final Map<Integer, ErrorCode> toEntityAttribute = new HashMap<>();

  public ErrorCodeConverter() {
    toDatabaseColumn.put(ErrorCode.UNKNOWN, 1);
    toDatabaseColumn.put(ErrorCode.BAD_REQUEST, 400);
    toDatabaseColumn.put(ErrorCode.FILE_NOT_FOUND, 404);

    toEntityAttribute.put(1, ErrorCode.UNKNOWN);
    toEntityAttribute.put(400, ErrorCode.BAD_REQUEST);
    toEntityAttribute.put(404, ErrorCode.FILE_NOT_FOUND);
  }

  @Override
  public Integer convertToDatabaseColumn(ErrorCode value) {
    if (value == null) {
      return null;
    }
    return toDatabaseColumn.get(value);
  }

  @Override
  public ErrorCode convertToEntityAttribute(Integer value) {
    if (value == null) {
      return null;
    }
    return toEntityAttribute.get(value);
  }
}
----

=== String Enum Converter with an ignored constant*

==== Source Enum

[source,java]
----
package org.example;

import de.schegge.enumconverter.WithEnumConverter;
import de.schegge.enumconverter.ConverterValue;

@WithEnumConverter(ordinal=false)
public enum ErrorCode {
  @ConverterValue(ignored=true)
  UNKNOWN,
  BAD_REQUEST,
  FILE_NOT_FOUND
}
----

==== Generated Converter

[source,java]
----
/**
* @author Enum Converter Generator by Jens Kaiser
* @version 2.0.0
*/
package org.example;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import java.util.Map;
import java.util.HashMap;

@Converter
public final class ErrorCodeConverter implements AttributeConverter<ErrorCode, String> {

  private final Map<ErrorCode, String> toDatabaseColumn = new HashMap<>();
  private final Map<String, ErrorCode> toEntityAttribute = new HashMap<>();

  public ErrorCodeConverter() {
    toDatabaseColumn.put(ErrorCode.BAD_REQUEST, "BAD_REQUEST");
    toDatabaseColumn.put(ErrorCode.FILE_NOT_FOUND, "FILE_NOT_FOUND");

    toEntityAttribute.put("BAD_REQUEST", ErrorCode.BAD_REQUEST);
    toEntityAttribute.put("FILE_NOT_FOUND", ErrorCode.FILE_NOT_FOUND);
  }

  @Override
  public String convertToDatabaseColumn(ErrorCode value) {
    if (value == null) {
      return null;
    }
    return toDatabaseColumn.get(value);
  }

  @Override
  public ErrorCode convertToEntityAttribute(String value) {
    if (value == null) {
      return null;
    }
    return toEntityAttribute.get(value);
  }
}
----

== WithEnumConverter Annotation

|===
|Attribut |Description |Type| Default

|`ordinal`|Creates an `AttributConverter` with Integer type (`true`) or String (`false`) as database type |`boolean`|`true`
|`autoApply`|Creates an "auto apply" (`true`) `AttributConverteror` or a normal one (`false`) |`boolean`|`false`
|`nullKeyForbidden`|Creates an `AttributConverter` which throws an `RuntimeException` on `null` parameters (`true`) or returns null (`false`) |`boolean`|`false`
|`exceptionIfMissing`|Creates an `AttributConverter` which throws an `RuntimeException` on `null` result (`true`) or returns `null` (`false`) |`boolean`|`false`
|===

== ConverterValue Annotation

|===
|Attribut |Description |Type| Default

|`ignored`|The annotated enum constant is ignored|`boolean`|`false`
|`value`|The values which are accepted from the database. Only the first value is written to the database|`String[]`|`{}`
|`include`|The value of the enum constant is added to the values (`true`) or not (`false`).|`boolean`|`false`
|===

Include defaults to `false` to avoid a breaking change.
